package main

import "fmt"

type me struct {
	name string
	age  int
}

func main() {
	obj := &me{"Alex", 25}
	fmt.Println(obj.name)
}
