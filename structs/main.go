package main

import "fmt"

type contactInfo struct {
	email   string
	zipcode int
}

type person struct {
	firstName string
	lastName  string
	contactInfo
}

func main() {
	alex := person{
		firstName: "Alex",
		lastName:  "Anderson",
		contactInfo: contactInfo{
			email:   "alex@anderson.com",
			zipcode: 12345,
		},
	}
	// alexpointer := &alex
	// alexpointer.updateName("Alex")
	// -------- or
	// (&alex).updateName("Alex")
	// -------- or
	alex.updateName("Alexey")
	alex.print()
}
func (p person) print() {
	fmt.Printf("%+v", p)
}

func (p *person) updateName(newFirstName string) {
	(*p).firstName = newFirstName
}
