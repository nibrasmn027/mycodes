abstract class Human{
    abstract void eat();
    abstract void sleep();
    void walk(){
        System.out.println("I am walking");
    }
}

class Student extends Human{
    void eat(){
        System.out.println("I am eating");
    }
    void sleep(){
        System.out.println("I am sleeping");
    }
}

public class Abstraction {
    public static void main(String[] args) {
        Human s = new Student();
        s.eat();
        s.walk();
        s.sleep();
    }
}
