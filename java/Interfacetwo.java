interface writer{
    void write();
}

class pen implements writer{
    public void write(){
        System.out.println("I am writing with Pen");
    }
}

class pencil implements writer{
    public void write(){
        System.out.println("I am writing with Pencil");
    }
}

class kit{
    public void write(writer p){
        p.write();
    }
}

public class Interfacetwo {
    public static void main(String[] args) {
        writer p = new pen();
        writer p1 = new pencil();
        kit k = new kit();
        k.write(p);
        k.write(p1);
    }
}
