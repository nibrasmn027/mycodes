interface Abc{
    void show();
}

class Xyz implements Abc{
    public void show(){
        System.out.println("Hello");
    }
    public void shi(){
        System.out.println("Hi");
    }
}


class Interface{
    public static void main(String[] args){
        System.out.println("Hello World");
        Abc obj = new Xyz();
        obj.show();
        Xyz x = new Xyz();
        x.shi();
    }
}