module pac

go 1.18

require (
	github.com/irai/arp v1.4.0
	github.com/j-keck/arping v1.0.2
)

require (
	github.com/mdlayher/arp v0.0.0-20191213142603-f72070a231fc // indirect
	github.com/mdlayher/ethernet v0.0.0-20190313224307-5b5fc417d966 // indirect
	github.com/mdlayher/raw v0.0.0-20190313224157-43dbcdd7739d // indirect
	golang.org/x/net v0.0.0-20190313220215-9f648a60d977 // indirect
	golang.org/x/sys v0.0.0-20200803210538-64077c9b5642 // indirect
)
