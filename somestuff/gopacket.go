package main

import (
	"context"
	"fmt"
	"log"
	"net"

	"github.com/irai/arp"
)

var mybyteslice = []byte("192.168.1.1")
var maskslice = []byte("24")

var ab = net.IPNet{mybyteslice, maskslice}

func main() {
	// var Debug = true
	fmt.Println(maskslice)
	fmt.Println(mybyteslice)

	a := arp.Handler{}
	s := context.Background()
	err := a.ScanNetwork(s, ab)
	if err != nil {
		log.Fatal("some thing went wrong", err)
	}

}
