package main

import (
	"log"
	"net"
)

func main() {
	a, b := net.ResolveIPAddr("", "192.168.1.3")
	log.Println(*a)
	log.Println(b)
}
