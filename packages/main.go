package main

import (
	"fmt"
	"learnpackage/simple"
)

func main() {
	fmt.Println("my own package")
	myvar := simple.Deck{"sumesh", 19}
	err := myvar.Print()
	if err != nil {
		fmt.Errorf("error is:", err)
	}
}
