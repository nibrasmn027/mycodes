package simple

import (
	"errors"
	"fmt"
)

// type lock interface {
// 	print(string, int) error
// }

type Deck struct {
	name string
	age  int
}

func SayHello() {
	fmt.Println("Hello")
}

func (d Deck) Print() (err error) {
	if d.name == "null" {
		err = errors.New("name can't be null")
		return
	}
	fmt.Printf("Entered name is %s and age is %d", d.name, d.age)
	return nil
}
