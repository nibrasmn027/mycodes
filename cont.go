package main

import (
	"context"
	"fmt"
	"log"
	"time"
)

func main() {
	ctx := context.Background()
	go contextTest(ctx, time.Second*2, "key")
	ctx.Done()
}

func contextTest(ctx context.Context, d time.Duration, key string) {
	time.Sleep(d)
	log.Println(ctx.Value(key))
	fmt.Println(key)
}

// empty
// context.Background()
// context.TODO()

// context.WithValue()
// context.WithDeadline()
// context.WithTimeout()
// context.WithCancel()
