package main

import "fmt"

func main() {
	//myMap := make(map[string]int)
	// var myMap map[string]int
	myMap := map[int]string{
		1: "one",
		2: "two",
		3: "three",
	}
	fmt.Println(myMap[1])
	delete(myMap, 1)
	fmt.Println(myMap)
}
