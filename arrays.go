package main

import "fmt"

func main() {

	var arr [8]int

	arr[3] = 23
	fmt.Println(len(arr))
	fmt.Println(arr[1])

	arr1 := [3]int{1, 23, 3}
	fmt.Println(arr1)
}

// var twoD [4][3] int
