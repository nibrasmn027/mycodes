package main

import (
	"errors"
)

type federal struct {
	balance float32
}

func Newfederal() *federal {
	return &federal{
		balance: 0,
	}
}

func (f *federal) deposit(amount float32) {
	f.balance += amount
}

func (f *federal) withdraw(amount float32) error {
	if f.balance < 0 || amount > f.balance {
		// log.Print("Insufficient funds")
		return errors.New("Insufficient funds")
	}
	// log.Println("Withdrawing", amount)
	f.balance = f.balance - amount
	return nil
}

func (f federal) getBalance() float32 {
	return f.balance
}
