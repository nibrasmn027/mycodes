package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
)

type logwriter struct{}

func main() {
	resp, err := http.Get("http://www.google.com")
	if err != nil {
		log.Fatal(err)
	}
	lw := logwriter{}
	// bs := make([]byte, 99999)
	// resp.Body.Read(bs)
	// log.Println(string(bs))
	io.Copy(lw, resp.Body)
}

func (logwriter) Write(bs []byte) (int, error) {
	fmt.Println(string(bs))
	fmt.Println("Just wrote this many bytes:", len(bs))
	return len(bs), nil
}
