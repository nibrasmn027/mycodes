package main

import "errors"

type BitCoinAccount struct {
	balance float32
	fee     float32
}

func NewBitCoinAccount() *BitCoinAccount {
	return &BitCoinAccount{
		balance: 0,
		fee:     50,
	}
}

func (b *BitCoinAccount) deposit(amount float32) {
	b.balance += amount
}

func (b *BitCoinAccount) withdraw(amount float32) error {
	if b.balance < 0 || amount > b.balance {
		return errors.New("Insufficient funds")
	}
	b.balance = b.balance - amount - b.fee
	return nil
}

func (b BitCoinAccount) getBalance() float32 {
	return b.balance
}
