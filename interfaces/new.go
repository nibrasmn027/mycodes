package main

import "fmt"

type bankAccount interface {
	deposit(amount float32)
	withdraw(amount float32) error
	getBalance() float32
}

func balanceBoth(ba bankAccount) {
	fmt.Println(ba.getBalance())
}

func main() {
	fd := federal{}
	fda := &fd
	bit := BitCoinAccount{}
	fda.deposit(100)
	bit.deposit(500)
	fmt.Println(fda.withdraw(2000))
	fmt.Println(fd.withdraw(20))
	fmt.Println(fda.getBalance())
	fmt.Println(bit.getBalance())
	// var ba bankAccount
	balanceBoth(fd)

}
