#include<iostream>
using namespace std;

int main(int argc, char **argv){
        if (argc < 2) {
        cerr << "go file not provided to power up!" << endl;
        exit(EXIT_FAILURE);
    }
    string cmd("sudo -E env \"PATH=$PATH\" go run ");
        if (argc == 2){
            cmd+=argv[1];
            system(cmd.c_str());
        } else if (argc == 3){
            cmd+=argv[1];
            cmd+=" ";
            cmd+=argv[2];
            system(cmd.c_str());
        } else if (argc == 4){
            cmd+=argv[1];
            cmd+=" ";
            cmd+=argv[2];
            cmd+=" ";
            cmd+=argv[3];
            system(cmd.c_str());
        } else if (argc == 5){
            cmd+=argv[1];
            cmd+=" ";
            cmd+=argv[2];
            cmd+=" ";
            cmd+=argv[3];
            cmd+=" ";
            cmd+=argv[4];
            system(cmd.c_str());
        } else if (argc == 6){
            cmd+=argv[1];
            cmd+=" ";
            cmd+=argv[2];
            cmd+=" ";
            cmd+=argv[3];
            cmd+=" ";
            cmd+=argv[4];
            cmd+=" ";
            cmd+=argv[5];
            system(cmd.c_str());
        } else if (argc == 7){
            cmd+=argv[1];
            cmd+=" ";
            cmd+=argv[2];
            cmd+=" ";
            cmd+=argv[3];
            cmd+=" ";
            cmd+=argv[4];
            cmd+=" ";
            cmd+=argv[5];
            cmd+=" ";
            cmd+=argv[6];
            system(cmd.c_str());
        }
      else {
            cout<<"Too many arguments"<<endl;
        }
        return EXIT_SUCCESS;

}
