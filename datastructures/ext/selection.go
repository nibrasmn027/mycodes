package main

func insertion(array []int) []int {
	var i int
	for i = 0; i < len(array); i++ {
		min := array[i]
		position := i
		var j = i + 1
		for j = i + 1; j < len(array); j++ {
			if array[j] < min {
				min = array[j]
				position = j
			}
		}
		array[position] = array[j]
		array[i] = min
	}
	return array
}

func main() {
	var arr [10]int = [10]int{12, 21, 7, 2, 32, 3, 78, 90, 9, 6}
	insertion(arr)
}
