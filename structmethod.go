package main

import "fmt"

type computer struct {
	manufacturer string
	ram          int
	cpu          string
	ssd          bool
}

func (c *computer) increaseram(amount int) {
	c.ram += amount
}

func main() {
	computer1 := computer{manufacturer: "Dell", ram: 32, cpu: "intel", ssd: true}
	fmt.Println(computer1)
	computer1.increaseram(8)
	fmt.Println(computer1)

}
