package main

import (
	"fmt"
	"time"
)

func attack(a string) {
	fmt.Println("attacking", a)

}
func main() {
	go attack("john")
	time.Sleep(time.Second)
	go attack("jane")
	time.Sleep(time.Second)
}
