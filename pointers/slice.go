package main

import "fmt"

func main() {
	mySlice := []int{1, 2, 3, 4, 5}
	updateMySlice(mySlice)
	fmt.Println(mySlice)
}
func updateMySlice(s []int) {
	s[0] = 10
}
