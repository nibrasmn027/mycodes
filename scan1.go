package main

import (
	"errors"
	"fmt"
)

func main() {
	var a int
	_, er := fmt.Scan(&a)
	fmt.Println(er)
	/*if er != nil {
		fmt.Println("error")
		fmt.Println(er)
	} else {
		fmt.Println(a)
	}*/
	ret, err := somefunc(a)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(ret)
	}

}
func somefunc(b int) (int, error) {
	if b == 60 {
		return -1, errors.New("we don't like 60")
	} else {
		return b + 78, nil
	}
}
