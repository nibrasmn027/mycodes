package main

import "fmt"

type Node struct {
	data int
	next *Node
}

func insertbeginning(head *Node, data int) *Node {
	// function to insert at the beginning
	var newnode *Node
	newnode = new(Node)
	newnode.data = data
	newnode.next = head
	head = newnode
	return head
}

func insertend(head *Node, data int) *Node {
	// function to insert at the end
	var newnode *Node
	newnode = new(Node)
	newnode.data = data
	newnode.next = nil
	if head == nil {
		head = newnode
		return head
	}
	var temp *Node
	temp = head
	for temp.next != nil {
		temp = temp.next
	}
	temp.next = newnode
	return head
}

func insertpos(head *Node, data int, pos int) *Node {
	// function to insert at a given position
	var newnode *Node
	newnode = new(Node)
	newnode.data = data
	if pos == 1 {
		newnode.next = head
		head = newnode
		return head
	}
	var temp *Node
	temp = head
	for i := 1; i < pos-1; i++ {
		temp = temp.next
	}
	newnode.next = temp.next
	temp.next = newnode
	return head
}

func deletebeginning(head *Node) *Node {
	// function to delete at the beginning
	if head == nil {
		fmt.Println("Empty list")
		return head
	}
	head = head.next
	return head
}

func deletepos(head *Node, pos int) *Node {
	// function to delete at a given position
	if head == nil {
		fmt.Println("Empty list")
		return head
	}
	if pos == 1 {
		head = head.next
		return head
	}
	var temp *Node
	temp = head
	for i := 1; i < pos-1; i++ {
		temp = temp.next
	}
	temp.next = temp.next.next
	return head
}

func deleteend(head *Node) *Node {
	// function to delete at the end
	if head == nil {
		fmt.Println("Empty list")
		return head
	}
	var temp *Node
	temp = head
	for temp.next.next != nil {
		temp = temp.next
	}
	temp.next = nil
	return head
}

func display(head *Node) {
	// function to display the list
	if head == nil {
		fmt.Println("Empty list")
		return
	}
	var temp *Node
	temp = head
	for temp != nil {
		fmt.Println(temp.data)
		temp = temp.next
	}
}

func main() {

	func() {
		// function to impliment linkedlist
		var head *Node
		head = nil
		fmt.Println("Linkedlist implimentation")
		fmt.Println("1. Insert at the beginning")
		fmt.Println("2. Insert at the end")
		fmt.Println("3. Insert at a given position")
		fmt.Println("4. Delete at the beginning")
		fmt.Println("5. Delete at the end")
		fmt.Println("6. Delete at a given position")
		fmt.Println("7. Display")
		fmt.Println("8. Exit")
		var choice int
		for {
			fmt.Println("Enter your choice")
			fmt.Scan(&choice)
			switch choice {
			case 1:
				fmt.Println("Enter the data")
				var data int
				fmt.Scan(&data)
				head = insertbeginning(head, data)
			case 2:
				fmt.Println("Enter the data")
				var data int
				fmt.Scan(&data)
				head = insertend(head, data)
			case 3:
				fmt.Println("Enter the data")
				var data int
				fmt.Scan(&data)
				fmt.Println("Enter the position")
				var pos int
				fmt.Scan(&pos)
				head = insertpos(head, data, pos)
			case 4:
				head = deletebeginning(head)
			case 5:
				head = deleteend(head)
			case 6:
				fmt.Println("Enter the position")
				var pos int
				fmt.Scan(&pos)
				head = deletepos(head, pos)
			case 7:
				display(head)
			case 8:
				fmt.Println("Exiting")
				return
			default:
				fmt.Println("Enter a valid choice")
			}
		}

	}()
	// linklist()
}
