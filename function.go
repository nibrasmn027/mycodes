package main

import "fmt"

func main() {
	fmt.Println("hello")
	fmt.Println(add(10, 90))
	fmt.Println(kwarg(10, 192, 1739, 183, 84))
}

func printstuff() {
	fmt.Println("nside print function")
}

func add(a int, b int) int {
	return a + b
}

func addt(a, b, c int) int {
	return a + b + c
}

func kwarg(numbers ...int) int {
	mysum := 0
	for _, number := range numbers {
		mysum = mysum + number
	}
	return mysum
}
