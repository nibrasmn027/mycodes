package main

import "fmt"

func main() {

	func(message string) {
		fmt.Println(message)

	}("hello world -> anonymous inside main")

	anon_print := return_anon()
	anon_print(9)
	next_int := ret_int()
	fmt.Println(next_int)
	fmt.Println(next_int())
	fmt.Println(next_int)
	fmt.Println(next_int())
	fmt.Println(next_int)

	//next_int2 := return_anon()
	//fmt.Println(next_int2())
}

func ret_int() func() int {

	i := 0
	return func() int {
		i++
		return i
	}
}

func return_anon() func(int) {
	return func(val int) {
		fmt.Println(val + 1)
	}
}
