package main

import (
	"fmt"
	"net/http"
	"time"
)

func main() {
	link := []string{
		"https://www.google.com",
		"https://www.facebook.com",
		"https://www.youtube.com",
		"https://www.instagram.com",
		"https://www.twitter.com",
		"https://www.linkedin.com",
		"https://www.github.com",
	}

	c := make(chan string)

	for _, url := range link {
		go checkLink(url, c)
	}
	for l := range c {
		go func(link string) {
			time.Sleep(5 * time.Second)
			checkLink(link, c)
		}(l)
	}
}

func checkLink(url string, c chan string) {
	_, err := http.Get(url)
	if err != nil {
		fmt.Println(url, "is down")
		c <- url
		return
	}
	fmt.Println(url, "is up")
	c <- url
}
