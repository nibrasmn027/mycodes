package main

import (
	"fmt"
	"math"
)

type shape interface {
	area() float64
}

type circle struct {
	radius float64
}

type rectangle struct {
	height float64
	width  float64
}

func (c *circle) area() float64 {
	return c.radius * 2 * math.Pi
}

func (r *rectangle) area() float64 {
	return r.height * r.width
}

func somefunction(someshape shape) {
	fmt.Println("area of shape rectangle is : ", someshape.area())
}

func main() {
	circle1 := circle{radius: 12}
	rectangle1 := rectangle{height: 12, width: 12}

	somefunction(&circle1)
	somefunction(&rectangle1)

}
