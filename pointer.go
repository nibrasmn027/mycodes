// Demostrate poiters in go and call by reference and call by value

package main

import "fmt"

func change_reference(val *int) {
	*val = 500
}

func change_value(val int) {
	val = 200
}

func main() {

	var a = 10
	ptr := &a
	fmt.Println(&a)
	fmt.Println(*ptr)

	fmt.Println("---------------------")

	fmt.Println("a is ", a)
	change_value(a)
	fmt.Println("a after change value is ", a)
	change_reference(&a)
	fmt.Println("a after change by reference is ", a)

	fmt.Println("---------------------")

	var pointer *int

	var b = 200

	pointer = &b

	fmt.Println(pointer)
	fmt.Println(*pointer)

}
