package main

import "fmt"

func main() {
	var myinput int

	fmt.Println("Input something")
	fmt.Scan(&myinput)
	fmt.Println(myinput)

	switch myinput {
	case 3:
		fmt.Println("this is one")
	case 2:
		fmt.Println("this is two")
	default:
		fmt.Println("wow")
	}
}
