package main

import "fmt"

type Class struct {
	x int
	y int
}

func main() {
	var somevar Class = Class{4, 5}
	fmt.Println(somevar)
	somevar.x = 10
	fmt.Println(somevar)
}
