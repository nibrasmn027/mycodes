#include<stdio.h>
#include<stdlib.h>

int main(){
    int i, j, o;
    for(i=1, o=1; i<=5; i++, o++){
        for(j=1; j<=i; j++){
            if (o%2!=0)
                printf("1 ");
            else
                printf("2 ");
        }
        printf("\n");
    }
    return 0;
}