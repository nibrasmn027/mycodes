#include<stdio.h>
#include<stdlib.h>

int main(){
    int i, j, n=5, o;
    for(i=1; i<n; i++){
        for(j=i; j<=n; j++){
            printf(" ");
        }
        for (j=1, o=1; j<=i; j++, o++){
            printf("%d ", o);
        }
        printf("\n");
    }
    for(i=1; i<=n; i++){
        for(j=1; j<=i; j++){
            printf(" ");
        }
        for (j=i, o=1; j<=n; j++, o++){
            printf("%d ", o);
        }
        printf("\n");
    }
    return 0;
}