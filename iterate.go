package main

import (
	"fmt"
)

func main() {

	numbers := []int{10, 20, 20, 40, 40, 60, 60}

	for index, number := range numbers {
		fmt.Printf("value of index %d is %d\n", index, number)
	}

	m1 := map[string]int{"key1": 1, "key2": 2, "key3": 3, "key4": 4}
	for key, value := range m1 {
		fmt.Printf("this is here %s and content %d \n", key, value)
	}
}
