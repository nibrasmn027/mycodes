package main

import "fmt"

func main() {
	var a int = 10
	fmt.Println(a)

	var b int
	fmt.Println(b)

	var c = 35
	fmt.Println(c)

	// Multiple variables of different types
	// are declared and initialized in the single line
	var myvariable1, myvariable2, myvariable3 = 2, "GFG", 67.56

	// Display the value and
	// type of the variables
	fmt.Printf("The value of myvariable1 is : %d\n",
		myvariable1)

	fmt.Printf("The type of myvariable1 is : %T\n",
		myvariable1)

	fmt.Printf("\nThe value of myvariable2 is : %s\n",
		myvariable2)

	fmt.Printf("The type of myvariable2 is : %T\n",
		myvariable2)

	fmt.Printf("\nThe value of myvariable3 is : %f\n",
		myvariable3)

	fmt.Printf("The type of myvariable3 is : %T\n",
		myvariable3)

	// Using short variable declaration
	myvar1 := 39
	myvar2 := "GeeksforGeeks"
	myvar3 := 34.67

	// Display the value and type of the variables
	fmt.Printf("The value of myvar1 is : %d\n", myvar1)
	fmt.Printf("The type of myvar1 is : %T\n", myvar1)

	fmt.Printf("\nThe value of myvar2 is : %s\n", myvar2)
	fmt.Printf("The type of myvar2 is : %T\n", myvar2)

	fmt.Printf("\nThe value of myvar3 is : %f\n", myvar3)
	fmt.Printf("The type of myvar3 is : %T\n", myvar3)
}

// go variables are
// basic:
// int : 8, 16..
// float : 8, 16..
// string
// complex : 8..
// bool
