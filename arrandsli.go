// arrays

var array1 [10]int
var array2 [10]int

array1[10] = 10

arr := [10]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

var twod [2][3]int

// slices
s1 := make([]int, 5)
append(s1, 1)

cp1 := make([]int, len(s1))

// map

m1 := make(map[string]int)
m1["a"] = 1
m1["b"] = 2
m1["c"] = 3

value, present := m1["a"]
delete(m1, "a")