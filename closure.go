package main

import "fmt"

func main() {
	nextevennumber := evennumbers()
	fmt.Println(nextevennumber())
	fmt.Println(nextevennumber())
	fmt.Println(nextevennumber())
	fmt.Println(nextevennumber())
	fmt.Println(nextevennumber())
	fmt.Println(nextevennumber())
}

func evennumbers() func() int {
	i := 0
	return func() int {
		i += 2
		return i
	}
}
