package main

import "fmt"

func main() {

	s1 := make([]int, 5)
	fmt.Println(s1)
	s1 = append(s1, 90)
	fmt.Println(s1)

	s2 := make([]int, 4)
	copy(s2, s1)
	fmt.Println(s2)
	fmt.Println(s2[1:2])
}
