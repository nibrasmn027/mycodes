package main

import "fmt"

func main() {
	fmt.Println("This is loop 0")
	x := 1
	for x < 10 {
		x++
		fmt.Printf("x is %d\n", x)
	}

	for x = 10; x <= 20; x++ {
		fmt.Println("x is ", x)
	}

}
