package main

import "fmt"

func main() {

	// Anonymous function
	func() {

		fmt.Println("This is an anonymous function")
	}()

	a := func(b int) int {
		return b + 200
	}
	g := a(6)
	fmt.Println(g)
	fmt.Println(a(g))

}
