package main

import (
	"fmt"
	"log"
	"net/http"
	"sync"
)

func main() {

	wg := sync.WaitGroup{}
	weblist := []string{
		"http://google.com",
		"http://facebook.com",
		"http://amazon.com",
		"http://yahoo.com",
		"http://bing.com",
		"http://twitter.com",
		"http://linkedin.com",
	}

	for _, endpoint := range weblist {
		go checkWeb(endpoint, &wg)
		wg.Add(1)
	}
	wg.Wait()
}
func checkWeb(endpoint string, wg *sync.WaitGroup) {
	defer wg.Done()
	res, err := http.Get(endpoint)
	if err != nil {
		log.Fatal(err)
		return
	}
	fmt.Printf("%s is up with status code of %d\n", endpoint, res.StatusCode)
}
