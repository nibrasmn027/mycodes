package main

import "fmt"

func main() {

	m1 := make(map[string]int)

	m1["key1"] = 2
	m1["key2"] = 3

	fmt.Println(m1)

	delete(m1, "key2")
	fmt.Println(m1)

}
