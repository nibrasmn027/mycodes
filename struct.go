package main

import "fmt"

type computer struct {
	manufacturer string
	ram          int
	cpu          string
	ssd          bool
}

// Constructor for struct -> computer
func compstruct(m string, r int, c string, s bool) *computer {
	cmp := computer{manufacturer: m, ram: r, cpu: c, ssd: s}
	return &cmp
}

// Default Constructor for struct -> computer
func compstructDefault() *computer {
	cmp := computer{manufacturer: "Default", ram: 4, cpu: "Default", ssd: false}
	return &cmp
}

func main() {
	// Simple instance of struct -> computer.
	computer1 := computer{manufacturer: "hp", ram: 16, cpu: "intel", ssd: true}
	fmt.Println(computer1)
	fmt.Println(computer1.ram)
	fmt.Println("----------------------------------------------")
	// Constructor
	computer2 := compstruct("acer", 32, "amd", true)
	fmt.Println(*computer2)
	fmt.Println("----------------------------------------------")
	// Default Constructor
	computer3 := compstructDefault()
	fmt.Println(*computer3)
}
